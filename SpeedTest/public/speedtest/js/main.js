$(function() {
    
    var module; // Current Network.js' module
    var rawModule;
    var keytoken = Math.random().toString(10).substr(2).slice(2,8);
    var supload;
    var sdownload;
    var sping;
    var sos;
    var sbrowser;
    var uploadn;
    var downloadn;




    /*
     * Tooltips
     */
 
    $('.btn-group').tooltip();


    /*
     * UI
     */
    
    var reloadFlag = false;

    var UI = {
        $btnStart: $('[data-measure]'),
        $btnAbort: $('[data-abort]'),
        $output: $('output'),

        start: function() {
            if (reloadFlag){
                location.reload();
            }
            //rawModule = $(this).data('measure');
            //module = rawModule.charAt(0).toUpperCase() + rawModule.slice(1);

            UI.$btnStart.prop('disabled', true);
            UI.$btnAbort.prop('disabled', false);
            
            var downTestWithCallback = function(callbackOption,callback){
                rawModule = 'download';
                module = 'Download';
                
                net.download.start();
                net.download.on('end', function(){
                    sdownload = (sdownload /1024/1024) * 8; 
                    sdownload = sdownload.toFixed(3);
                    downloadn = sdownload;
                    sdownload = sdownload + " Mbps";
                    document.getElementById('speedo-dl').innerHTML = downloadn;
                    callback(callbackOption);
                });
                

                
            }
            var upTestWithCallback = function(callback){
                rawModule = 'upload';
                module = 'Upload';
                net.upload.start();
                net.upload.on('end', function(){
                    supload = (supload /1024/1024)* 8; 
                    supload = supload.toFixed(3);
                    uploadn = supload;
                    supload = supload + " Mbps";
                    document.getElementById('speedo-up').innerHTML = uploadn;
                    callback();
                });
                
            }
            var latencyCheck = function(){
                rawModule = 'latency';
                module = 'Latency';
                net.latency.start();
		document.getElementById('speedo-status').innerHTML = "Pinging";
            }
            
            downTestWithCallback(latencyCheck,upTestWithCallback);

        },

        restart: function(size) {
            UI.notice(UI.delimiter(
                'The minimum delay of ' + UI.value(8, 'seconds') + ' has not been reached'
            ));

            UI.notice(UI.delimiter(
                'Restarting measures with '
                + UI.value(size / 1024 / 1024, 'MB')
                + ' of data...'
            ));
        },

        stop: function() {
            UI.notice(UI.delimiter('Finished measures'));
            
            UI.$btnAbort.prop('disabled', true);
        },

        abort: function() {
            net.upload.abort();
            net.download.abort();
        },

        notice: function(text, newSection) {
            var $o = UI.$output,
                stickToBottom = ($o.scrollTop() + $o.outerHeight()) == $o.prop('scrollHeight');

            $o.append('<br>');
            newSection && $o.append('<br>');

            $o.append('<span class="yellow">[' + module + ']</span> ' + text);
         

            if (stickToBottom) {
                $o.scrollTop($o.prop('scrollHeight'));
            }
        },

        value: function(value, unit) {
            if (value != null) {
                return '<span class="blue">' + value.toFixed(3) + ' ' + unit + '</span>';
            } else {
                return '<span class="blue">null</span>';
            }
        },

        delimiter: function(text) {
            return '<span class="green">' + text + '</span>';
        }
    };

    /*
     * Network.js configuration
     */

    var net = new Network();

    function start(size) {
        UI.notice(UI.delimiter(
            'Starting ' + rawModule + ' measures'
            + (rawModule != 'latency' ? (' with ' + UI.value(size / 1024 / 1024, 'MB') + ' of data') : '')
            + '...'
        ), true);
    }

    function progress(avg, instant) {
        var output = 'Instant speed: ' + UI.value(instant / 1024 / 1024, 'MBps');
        document.getElementById('speedo-status').innerHTML = module + "ing";
        $('#gauge').jqxGauge('value', (instant/1024/1024)*8 );
            output += ' // Average speed: ' + UI.value(avg / 1024 / 1024, 'MBps');
        $('#gauge').jqxGauge('value', (avg/1024/1024)*8 );
        UI.notice(output);
    }

    function end(avg) {
        UI.notice('Final average speed: ' + UI.value(avg / 1024 / 1024, 'MBps'));
        var OSName=navigator.appVersion;
        if (navigator.appVersion.indexOf("Win")!=-1) OSName="Windows";
        if (navigator.appVersion.indexOf("Mac")!=-1) OSName="MacOS";
        if (navigator.appVersion.indexOf("X11")!=-1) OSName="UNIX";
        if (navigator.appVersion.indexOf("Linux")!=-1) OSName="Linux";
        if (navigator.appVersion.indexOf("Android")!=-1) OSName="Android";
        if (navigator.appVersion.indexOf("iPHONE")!=-1) OSName="iPHONE";
        if (navigator.appVersion.indexOf("iPAD")!=-1) OSName="iPAD";
        if (navigator.appVersion.indexOf("iPOD")!=-1) OSName="iPOD";


        sos = OSName
        if(navigator.userAgent.indexOf("Chrome") != -1 ) 
            {
                 
                 sbrowser = 'chrome';

            }
            else if(navigator.userAgent.indexOf("Opera") != -1 )
            {
             
             sbrowser = 'opera';
            }
            else if(navigator.userAgent.indexOf("Firefox") != -1 ) 
            {
                 
                  sbrowser = 'firefox';
            }
            else if(navigator.userAgent.indexOf("Safari") != -1 ) 
            {
                 
                  sbrowser = 'safari';
            }
            else if((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true )) //IF IE > 10
            {
               
               sbrowser = 'ie';
            }  
            else 
            {
                
                sbrowser = navigator.userAgent;
            }
            
          // sbrowser = navigator.userAgent  
            if (module == "Download") {
            sdownload = avg;
         
        }
        else{
            supload = avg;
           
        }

        UI.stop();
    }

    net.upload.on('start', start).on('progress', progress).on('restart', UI.restart).on('end', end);
    net.download.on('start', start).on('progress', progress).on('restart', UI.restart).on('end', end);

    net.latency
        .on('start', start)
        .on('end', function(avg, all) {
            
	    reloadFlag = true;
            $("#starttest").html("Restart");
            var pingarray = []
            var jitter =[]
            for (i in all){pingarray.push(all[i].toFixed(3))}
            for (i in all){jitter.push(all[i].toFixed(3))}
            //pingarray = all;
            //jitter = all;
            
            for (i = 0; i < jitter.length; i++) {
                 jitter[i] = jitter[i]-avg;
                 jitter[i].toFixed(3);
            }

            all = all.map(function(latency) {
                return UI.value(latency, 'ms');
            });

            all = '[ ' + all.join(' , ') + ' ]';

            UI.notice('Instant latencies: ' + all);
            UI.notice('Average latency: ' + UI.value(avg, 'ms'));
            UI.notice("------------------------------------------")
            UI.notice("------------------------------------------")
            UI.notice('<span class="red">Your Refrence Number is : </span>' + keytoken)
            UI.notice("------------------------------------------")
            UI.notice("------------------------------------------")

            UI.stop();
	    
	    if (avg == null){
		avg = 0;
	    }
            sping = avg.toFixed(2) + ' ms';
            document.getElementById('status').innerHTML = "Latency is : " + sping;



            document.getElementById('provider').innerHTML = 'Your Refrence number is : ' + keytoken;
            sdata = {skey:keytoken,sos:sos,sbrowser:sbrowser,supload:supload,sdownload:sdownload,uip:userip,sping:sping,pingarray:pingarray,jitter:jitter,user_id:username,latitude:lat,longitude:long};
	    UI.$btnStart.prop('disabled', false);

            $.ajax({

                 url: "/scans.json",

                 type: "POST",

               data: JSON.stringify(sdata),
              // data : sdata,

                 contentType: "application/json",

            //     success: function(data) {

//                 }

            });      

        }); 

    /*
     * Bindings
     */

    $("#starttest").on('click', UI.start);
    //UI.$btnStart.on('click', UI.start);
    UI.$btnAbort.on('click', UI.abort);

});
