json.array!(@scans) do |scan|
  json.extract! scan, :id, :skey, :sos, :sbrowser, :supload, :sdownload, :uip, :sping, :pingarray, :jitter, :user_id , :pinglist
  json.url scan_url(scan, format: :json)
end
