class ApplicationController < ActionController::Base
   before_action :authenticate_user!
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
 # protect_from_forgery with: :null_session#
  # TODO : Resolve XSRF Issue
  protect_from_forgery with: :exception
  skip_before_action :verify_authenticity_token
  before_action :set_locale
  protected
  def set_locale
    I18n.locale = I18n.default_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end
  def default_url_options(options={})
    { :locale => I18n.locale }.merge options
  end
end
