class AddAddresToScans < ActiveRecord::Migration
  def change
    add_column :scans, :address, :string
  end
end
