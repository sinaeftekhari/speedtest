class AddLongitudeToScans < ActiveRecord::Migration
  def change
    add_column :scans, :longitude, :string
  end
end
