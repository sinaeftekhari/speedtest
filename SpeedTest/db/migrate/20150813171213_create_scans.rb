class CreateScans < ActiveRecord::Migration
  def change
    create_table :scans do |t|
      t.string :skey
      t.string :sos
      t.string :sbrowser
      t.string :supload
      t.string :sdownload
      t.string :uip
      t.string :sping
      t.string :pingarray
      t.string :jitter
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
