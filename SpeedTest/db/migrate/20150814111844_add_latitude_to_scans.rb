class AddLatitudeToScans < ActiveRecord::Migration
  def change
    add_column :scans, :latitude, :string
  end
end
