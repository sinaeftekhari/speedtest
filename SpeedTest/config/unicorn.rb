# Set the working application directory
# working_directory "/path/to/your/app"
working_directory "/home/stcustomer"

# Unicorn PID file location
# pid "/path/to/pids/unicorn.pid"
pid "/home/stcustomer/pids/unicorn.pid"

# Path to logs
# stderr_path "/path/to/log/unicorn.log"
# stdout_path "/home/customer/unicorn.log"
stderr_path "/home/stcustomer/log/unicorn.log"
stdout_path "/home/stcustomer/log/unicorn.log"

# Unicorn socket
listen "/tmp/unicorn.stcustomer.sock"
listen "/tmp/unicorn.myapp.sock"

# Number of processes
# worker_processes 4
worker_processes 2

# Time-out
timeout 30
