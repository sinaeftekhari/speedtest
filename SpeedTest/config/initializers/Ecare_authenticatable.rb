module Devise
  module Strategies
    class EcareAuthenticatable < Authenticatable
      def authenticate!
        if params[:user]
          url = URI("http://92.42.51.112:5004/services/MTNIranCell_Proxy?WSDL=")
          user = params[:user][:username]
          password = params[:user][:password]
          http = Net::HTTP.new(url.host, url.port)
          euser = "#{user}"
          epassword = "#{password}"
          wuser = "\"#{user}\""
          wpassword = "\"#{password}\""
          request = Net::HTTP::Post.new(url)
          request["content-type"] = 'text/xml'
          if euser.match(/^09411/) 
               request.body = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:open=\"http://www.openuri.org/\" xmlns:env=\"http://eai.mtnn.iran/Envelope\" xmlns:wim=\"http://eai.mtn.iran/Wimax\">\n <soapenv:Header>\n <pr:authentication soapenv:actor=\"http://schemas.xmlsoap.org/soap/actor/next\" soapenv:mustUnderstand=\"0\" xmlns:pr=\"http://webservices.irancell.com/ProxyService\">\n<pr:user>mw_spt</pr:user>\n<pr:password>MW_spt046</pr:password>\n</pr:authentication>\n</soapenv:Header>\n   <soapenv:Body>\n      <open:clientRequest>\n         <env:EaiEnvelope>\n            <env:Domain>1</env:Domain>\n            <env:Service>WimaxService</env:Service>\n            <env:ServiceId>1</env:ServiceId>\n            <env:Operation>getQueryTransactionDetails</env:Operation>\n            <env:Language>En</env:Language>\n            <env:UserId>abl_ssp</env:UserId>\n            <env:Sender>abl_ssp</env:Sender>\n            <env:MessageId>1</env:MessageId>\n            <env:Payload>\n               <wim:Wimax>\n                  <wim:Request>\n                     <wim:Operation_Name>getQueryTransactionDetails</wim:Operation_Name>\n                     <wim:XmlData_Input>\n                        <wim:XmlData>&lt;EVENT&gt;&lt;REQUEST ENTITY_ID=#{wuser} INFO_LEVEL=\"3\" PASSWORD=#{wpassword} EXTERNAL_USER=\"abl_scp\" EXTERNAL_APPLICATION=\"abl_scp\" EXTERNAL_REFERENCE=\"WMX\" REQUEST_TYPE=\"AUTH\"/&gt;&lt;/EVENT&gt;</wim:XmlData>\n                     </wim:XmlData_Input>\n                 </wim:Request>\n               </wim:Wimax>\n            </env:Payload>\n         </env:EaiEnvelope>\n      </open:clientRequest>\n   </soapenv:Body>\n</soapenv:Envelope>"
          else
               request.body = "<soap:Envelope soap:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\r   <soap:Header>\r      <pr:authentication xmlns:pr=\"http://webservices.irancell.com/ProxyService\">\r         <pr:user>mw_spt</pr:user>\r         <pr:password>MW_spt046</pr:password>\r      </pr:authentication>\r   </soap:Header>\r   <soap:Body>\r      <open:clientRequest xmlns:open=\"http://www.openuri.org/\" xmlns:env=\"http://eai.mtnn.iran/Envelope\">\r         <env:EaiEnvelope>\r            <env:Domain>1</env:Domain>\r            <env:Service>Ecare</env:Service>\r            <env:Language>en</env:Language>\r            <env:UserId>#{euser}</env:UserId>\r            <env:Sender>speedtest</env:Sender>\r            <env:MessageId>062222014</env:MessageId>\r            <env:Payload>\r               <ec:EcareData xmlns:ec=\"http://eai.mtn.iran/Ecare\">\r                  <ec:Request>\r                     <ec:Operation_Name>authenticateCustomer</ec:Operation_Name>\r                     <ec:CustDetails_InputData>\r                        <ec:MSISDN>#{euser}</ec:MSISDN>\r                        <ec:newPassword>#{epassword}</ec:newPassword>\r                        <ec:language>En</ec:language>\r                     </ec:CustDetails_InputData>\r                  </ec:Request>\r               </ec:EcareData>\r            </env:Payload>\r         </env:EaiEnvelope>\r      </open:clientRequest>\r   </soap:Body>\r</soap:Envelope>" 
             # request.body = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:open=\"http://www.openuri.org/\" xmlns:env=\"http://eai.mtnn.iran/Envelope\" xmlns:wim=\"http://eai.mtn.iran/Wimax\">\n <soapenv:Header>\n <pr:authentication soapenv:actor=\"http://schemas.xmlsoap.org/soap/actor/next\" soapenv:mustUnderstand=\"0\" xmlns:pr=\"http://webservices.irancell.com/ProxyService\">\n<pr:user>mw_spt</pr:user>\n<pr:password>MW_spt046</pr:password>\n</pr:authentication>\n</soapenv:Header>\n   <soapenv:Body>\n      <open:clientRequest>\n         <env:EaiEnvelope>\n            <env:Domain>1</env:Domain>\n            <env:Service>WimaxService</env:Service>\n            <env:ServiceId>1</env:ServiceId>\n            <env:Operation>getQueryTransactionDetails</env:Operation>\n            <env:Language>En</env:Language>\n            <env:UserId>abl_ssp</env:UserId>\n            <env:Sender>abl_ssp</env:Sender>\n            <env:MessageId>1</env:MessageId>\n            <env:Payload>\n               <wim:Wimax>\n                  <wim:Request>\n                     <wim:Operation_Name>getQueryTransactionDetails</wim:Operation_Name>\n                     <wim:XmlData_Input>\n                        <wim:XmlData>&lt;EVENT&gt;&lt;REQUEST ENTITY_ID=#{euser} INFO_LEVEL=\"3\" PASSWORD=#{epassword} EXTERNAL_USER=\"abl_scp\" EXTERNAL_APPLICATION=\"abl_scp\" EXTERNAL_REFERENCE=\"WMX\" REQUEST_TYPE=\"AUTH\"/&gt;&lt;/EVENT&gt;</wim:XmlData>\n                     </wim:XmlData_Input>\n                 </wim:Request>\n               </wim:Wimax>\n            </env:Payload>\n         </env:EaiEnvelope>\n      </open:clientRequest>\n   </soapenv:Body>\n</soapenv:Envelope>"
          end
          p request.body
          response = http.request(request)
          p response.body
          @doc = Nokogiri::XML(response.body)
          @doc.remove_namespaces!
          result = @doc.xpath('//resultCode')
          auth = result[0].content.to_i
          if auth == 0
            user = User.find_or_create_by(username: user)
            success!(user)
            puts ("success login")
          else





            fail(:invalid_login)
          end


        end
      end

      def username
        params[:user][:username]
      end

    end
  end
end

Warden::Strategies.add(:ecare_authenticatable, Devise::Strategies::EcareAuthenticatable)
