require 'i18n'
I18n.default_locale = :en
RailsAdmin.config do |config|

  ### Popular gems integration
  config.authorize_with do |controller|
    unless current_user.try(:admin?)
      flash[:error] = "You are not an admin"
      redirect_to '/'
    end
  end
  ## == Devise ==
   config.authenticate_with do
    warden.authenticate! scope: :user
   end



  config.navigation_static_label = "<b>My Links</b>"
  config.navigation_static_links = {
      'E-Care Login' => 'http://ecare.irancell.ir',
      'E-Care Password Request' => 'http://ecare.irancell.ir/appmanager/sspportal/login'
  }

  #config.current_user_method(&:current_user)

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
     history_index
     history_show
  end
end
